# corona

A tool to quickly gather patient reported reported symptoms of respitary problems.

## Online demo

Click around [the the live demo of current master branch](https://corona-symptoms-reporter.herokuapp.com/).

# How to run docker with docker compose

```
docker-compose up
```

## Translations with docker

Generate new messages
```
docker-compose run --entrypoint django-admin web makemessages -l <lang code, e.g. fi, see settings.py for supported>
```

Once filled into the locale files compile new translations with
```
docker-compose run --entrypoint django-admin web compilemessages
```

# How to run locally for dev

create virtualenv for dependencies
```
virtualenv -p python3 .venv
```

Install dependencies

```
. .venv/bin/activate.fish
pip3 install -r requirements.txt
```

Install captcha dependencies

```
apt-get -y install libz-dev libjpeg-dev libfreetype6-dev python-dev gettext libgettextpo-dev
```

Setup local database file
```
echo 'DATABASE_URL=sqlite:///db.sqlite3' > .env
```

run migrations:
```
python3 manage.py migrate
```


Run project
```
# in the earlier virtualenv
python3 manage.py runserver
```
## Translations

Generate new messages
```
django-admin makemessages -l <lang code, e.g. fi, see settings.py for supported>
```

Once filled into the locale files compile new translations with
```
django-admin compilemessages
```

# How to use admin

## Create admin user
```
# In venv
python3 manage.py createsuperuser
```

# Stuff available in admin
* Generate test data (random answers)
* Disable 24h reporting cooldown perioid

# Configuration

Iframe-security can be relaxed with environment variable `ALLOW_ALL_IFRAMES`:

```
export ALLOW_ALL_IFRAMES=True
```

#!/bin/bash

# Collect static files
echo "Collect static files"
python manage.py collectstatic --noinput

# Collect static files
echo "Compile translation messages"
python manage.py compilemessages

# Apply database migrations
echo "Apply database migrations"
until python manage.py migrate; do sleep 1; done

# Start server
echo "Starting server"
python manage.py $@

FROM python:3.7.6

RUN apt-get update && apt-get -y install libz-dev libjpeg-dev libfreetype6-dev gettext libgettextpo-dev

WORKDIR /app
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY corona/ ./
COPY docker-entrypoint.sh /

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["runserver", "0.0.0.0:8080"]

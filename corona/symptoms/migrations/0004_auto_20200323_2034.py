# Generated by Django 3.0.3 on 2020-03-23 20:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('symptoms', '0003_auto_20200323_1940'),
    ]

    operations = [
        migrations.AlterField(
            model_name='symptomsreport',
            name='age',
            field=models.CharField(blank=True, choices=[('AGE_UNDER_12', 'ageUnder12'), ('AGE_12_17', 'age12_17'), ('AGE_18_24', 'age18_24'), ('AGE_25_34', 'age25_34'), ('AGE_35_44', 'age35_44'), ('AGE_45_54', 'age45_54'), ('AGE_55_64', 'age55_64'), ('AGE_65_74', 'age65_74'), ('75_OR_OLDER', 'age75OrOlder')], default=None, max_length=100, null=True, verbose_name='modelFieldSymptomsReportAge'),
        ),
    ]

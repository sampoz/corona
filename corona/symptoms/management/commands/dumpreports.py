import os
import time
import gzip
from tqdm import tqdm
from django.core.management.base import BaseCommand, CommandError
from symptoms.models import SymptomsReport


class Command(BaseCommand):
    help = 'Dumps the current symptoms report database to a JSON file in JSON lines format'

    def add_arguments(self, parser):
        parser.add_argument(
            '--path',
            type=str,
            required=True,
            help='Path to the JSON data dump')
        parser.add_argument(
            '--verbose',
            action='store_true',
            help='Show progress')
        parser.add_argument(
            '--rehash',
            action='store_true',
            help='Rehash the hashed report values with random salt')
        parser.add_argument(
            '--format',
            type=str,
            choices=['json', 'gzip'],
            default='json',
            help='Save format')

    def handle(self, *args, **options):
        # Store the verbose argument value
        self.verbose = options['verbose']
        start_time = time.time()

        try:
            path = options['path']
            rehash = options['rehash']
            format = options['format']
            rehash_salt = os.urandom(64).hex() if rehash else None
            self._print('Using rehash salt: {}'.format(rehash_salt))
            self._print('Starting to dump symptoms report data in format: {} to: {}'
                        .format(format, os.path.abspath(path)))

            if format == 'json':
                f = open(path, 'w', encoding='utf-8')
            elif format == 'gzip':
                f = gzip.open(path, 'wt', encoding='utf-8')
            else:
                raise CommandError('Unknown format: {}'.format(format))

            with f:
                for report in tqdm(SymptomsReport.objects.all(), disable=not self.verbose):
                    f.write(report.to_json(rehash_salt=rehash_salt) + '\n')

            self._print('Dumped data for {} symptoms reports'.format(SymptomsReport.objects.count()))
            self._print('Symptoms report dump completed in: {} seconds'.format(time.time() - start_time))
        except Exception as e:
            raise CommandError('Failed to dump symptoms report data: {}'.format(e)) from e

    def _print(self, msg):
        if self.verbose:
            self.stdout.write(msg)

import json
import logging
import os
import random
import uuid

from django.utils import timezone
from django.apps import AppConfig
from django.conf import settings
from django.db.utils import DatabaseError

# Get an instance of a logger
logger = logging.getLogger(__name__)


class SymptomsConfig(AppConfig):
    name = 'symptoms'

    def ready(self):
        try:
            self._load_municipalities_from_json(os.path.join(settings.BASE_DIR, 'static', 'finland-municipalities-topojson.json'))
            self._load_post_code_areas_from_json(os.path.join(settings.BASE_DIR, 'static', 'postal_code_data.json'))
            #self.generate_test_data(10000)
        except Exception as e:
            logger.warning("Failed to load data from JSON: {} - if you are running migrations ignore this warning"
                           .format(e))

    def generate_test_data(self, num):
        from .models import SymptomsReport, Symptom, PostCodeArea

        logger.info("Generating {} random symptoms reports".format(num))

        post_code_areas = list(PostCodeArea.objects.all())
        non_symptomatic_perc = 0.3
        min_symptoms = 1
        max_symptoms = 5

        for i in range(num):
            post_code_area = post_code_areas[random.randint(0, len(post_code_areas)-1)]
            has_symptoms = random.random() > non_symptomatic_perc
            symptoms = []

            if has_symptoms:
                num_symptoms = random.randint(min_symptoms, max_symptoms)
                for i in range(num_symptoms):
                    symptom = Symptom.SymptomName.choices[random.randint(1, len(Symptom.SymptomName.choices)-1)][0]
                    symptoms.append(symptom)
            else:
                symptoms.append(Symptom.SymptomName.NONE)

            created_at = timezone.now()
            user_cookie = uuid.uuid4()

            symptoms_report = SymptomsReport(
                post_code_area=post_code_area,
                created_at=created_at,
                user_cookie=user_cookie
            )
            symptoms_report.save()

            for symptom_name in symptoms:
                symptom = Symptom(
                    name=symptom_name,
                    symptoms_report=symptoms_report
                )
                symptom.save()

        logger.info("Generated {} random symptoms reports".format(num))


    def _load_municipalities_from_json(self, json_path: str):
        from .models import Municipality

        if Municipality.objects.all().count() == 0:
            logger.warning('No Municipality objects found in the database - loading data from JSON: {}'
                           .format(json_path))

            with open(json_path, 'r', encoding='utf-8') as f:
                data = json.load(f)

            municipality_geometries = data['objects']['kuntarajat']['geometries']
            logger.info('Found {} Municipality object data from JSON'.format(len(municipality_geometries)))

            for mg in municipality_geometries:
                name = mg['properties']['name']
                code = int(mg['properties']['code'])

                municipality_model = Municipality(
                    name=name,
                    code=code)
                municipality_model.save()

            logger.info('Loaded {} Municipality objects from JSON'.format(Municipality.objects.all().count()))

    def _load_post_code_areas_from_json(self, json_path: str):
        from .models import Municipality
        from .models import PostCodeArea

        if PostCodeArea.objects.all().count() == 0:
            logger.warning('No PostCodeArea objects found in the database - loading data from JSON: {}'
                           .format(json_path))

            with open(json_path, 'r', encoding='utf-8') as f:
                data = json.load(f)

            logger.info('Found {} PostCodeArea object data from JSON'.format(len(data.keys())))

            for key in data.keys():
                pca = data.get(key)[0]
                postal_code = pca['pnro']
                name = pca['name']
                municipality_name = pca['municipality']
                population = pca['population']
                area = pca['area_km2']

                try:
                    municipality = Municipality.objects.get(name=municipality_name)
                except Exception as e:
                    logger.error("Could not find Municipality object with name: {}. Error: {}"
                                 .format(municipality_name, e))
                    continue

                try:
                    pca_model = PostCodeArea(
                        postal_code=postal_code,
                        name=name,
                        municipality=municipality,
                        population=population,
                        area=area)

                    pca_model.save()
                except Exception as e:
                    logger.error("Failed to create PostCodeArea object from JSON data: {}. Error: {}".format(pca, e))

            logger.info("Loaded {} PostCodeArea objects from JSON".format(PostCodeArea.objects.all().count()))

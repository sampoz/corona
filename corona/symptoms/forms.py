import logging

from django import forms
from django.utils import timezone
from django.forms import ModelForm, CharField, MultipleChoiceField, DateTimeField, IntegerField
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ValidationError
from captcha.fields import CaptchaField
from .models import SymptomsReport, Symptom, PostCodeArea
from . import constants


# Get an instance of a logger
logger = logging.getLogger(__name__)


class SymptomsReportForm(ModelForm):

    captcha = CaptchaField()

    postal_code = CharField(
        min_length=5,
        max_length=5,
        required=True,
        strip=True,
        label=_('formFieldPostalCode'),
    )

    symptoms = MultipleChoiceField(
        choices=Symptom.SymptomName.choices,
        widget=forms.CheckboxSelectMultiple,
        label=_('formFieldSymptoms'),
    )

    # Override to enforce range [min, max] in frontend
    duration = IntegerField(
        required=False,
        label=_('formFieldDuration'),
        min_value=constants.MIN_DURATION,
        max_value=constants.MAX_DURATION
    )

    # Override to enforce range [min, max] in frontend
    size_of_household = IntegerField(
        required=False,
        label=_('formFieldSizeOfHousehold'),
        min_value=constants.MIN_SIZE_OF_HOUSEHOLD,
        max_value=constants.MAX_SIZE_OF_HOUSEHOLD)

    fill_start_time = DateTimeField(
        initial=timezone.now(),
        required=False,
        widget=forms.HiddenInput(),
    )

    field_order = ['postal_code', 'symptoms', 'tested_for_covid19', 'duration', 'size_of_household', 'age', 'sex']

    def clean_postal_code(self):
        data = self.cleaned_data.get('postal_code')

        if data is None:
            raise ValidationError(_("formErrorNoPostalCode"))

        try:
            post_code_area = PostCodeArea.objects.get(postal_code=data)
            logger.debug("Found matching post code area: {} -> {}".format(data, post_code_area))
        except PostCodeArea.DoesNotExist:
            raise ValidationError(_("formErrorInvalidPostalCode"))

        return data

    def clean_symptoms(self):
        data = self.cleaned_data.get('symptoms', [])

        if len(data) == 0:
            raise ValidationError(_("formErrorNoSymptoms"))

        if 'NONE' in data and len(data) > 1:
            raise ValidationError(_("formErrorNoSymptomsCombined"))

        return data

    class Meta:
        model = SymptomsReport
        exclude = ['fill_start_time', 'post_code_area', 'user_cookie', 'created_at', 'fill_time_sec',
                   'user_cookie_hash', 'client_user_agent_hash', 'client_ip_hash', 'client_geoip_country_code']
        required = ['postal_code', 'symptoms']

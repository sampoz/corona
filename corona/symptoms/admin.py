from django.contrib import admin

# Register your models here.

from .models import SymptomsReport
from .apps import SymptomsConfig
from django.apps import apps


def generate_test_data(modeladmin, request, queryset):
    symptoms_config = apps.get_app_config('symptoms')
    symptoms_config.generate_test_data(100)

class SymptomsAdmin(admin.ModelAdmin):
    actions = [generate_test_data]

admin.site.register(SymptomsReport, SymptomsAdmin)

import json
import logging
import uuid

from typing import Union

from waffle import flag_is_active

from django.core.serializers.json import DjangoJSONEncoder
from django.core.signing import BadSignature
from django.db.models import Count
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.template import loader
from django.urls import reverse
from django.utils import timezone
from django.utils.encoding import force_text
from django.utils.functional import Promise
from django.contrib.gis.geoip2 import GeoIP2
from django.conf import settings

from ipware import get_client_ip

from . import constants
from .utils import _get_hashed_value
from .forms import SymptomsReportForm
from .models import PostCodeArea, Symptom, SymptomsReport


# Get an instance of a logger
logger = logging.getLogger(__name__)


_GEO_IP2 = GeoIP2()
_POSTAL_CODE_TO_SAFE_POSTAL_CODE = {}


class GetTextLazyEncoder(DjangoJSONEncoder):
    def default(self, obj):
        if isinstance(obj, Promise):
            return force_text(obj)
        return super(GetTextLazyEncoder, self).default(obj)


def index(request):
    template = loader.get_template('SymptomsReport/index.html')
    context = {
        'symptom_report_json': json.dumps(_municipality_symptom_report()),
        'translations_json': json.dumps(_municipality_symptom_report_translations(), cls=GetTextLazyEncoder),
    }
    return HttpResponse(template.render(context, request))


def detail(request, symptomreport_id):
    return HttpResponse("You're looking at SymptomReport %s. Symptom was" % symptomreport_id)


def report(request):
    previous_symptoms_report = None

    try:
        user_cookie = request.get_signed_cookie(constants.USER_COOKIE_NAME)
        if user_cookie:
            previous_symptoms_report = _get_previous_user_symptoms_report(user_cookie)
    except (KeyError, BadSignature) as e:
        logger.warning("Failed to get user cookie: {}".format(e))

    data = None

    if previous_symptoms_report:
        logger.info("Found latest symptoms report for user: {}. Report: {}"
                    .format(previous_symptoms_report.user_cookie, previous_symptoms_report))

        # Check that the cooldown has passed if not redirect the user to the cooldown page
        if not _reporting_cooldown_passed(request ,previous_symptoms_report):
            template = loader.get_template('SymptomsReport/cooldown.html')
            delta = timezone.now() - previous_symptoms_report.created_at
            context = {
                'cooldown_period_hours': int(constants.SYMPTOM_REPORT_COOLDOWN_SEC/(60*60)),
                'cooldown_remaining_hours': int(round((constants.SYMPTOM_REPORT_COOLDOWN_SEC-delta.total_seconds())/(60*60)))
            }
            return HttpResponse(template.render(context, request))

        # If cooldown has passed do the user a favour and prepopulate the fields with previous responses
        symptoms = list(previous_symptoms_report.symptoms.all())
        symptoms = [s.name for s in symptoms]
        data = {
            'postal_code': previous_symptoms_report.post_code_area.postal_code,
            'symptoms': symptoms,
            'duration': previous_symptoms_report.duration,
            'age': previous_symptoms_report.age,
            'sex': previous_symptoms_report.sex,
        }

    # TODO Plz fix urls and namespaces
    template = loader.get_template('SymptomsReport/report.html')
    context = {'form': SymptomsReportForm(initial=data)}
    return HttpResponse(template.render(context, request))


def reportsubmit(request):
    form = SymptomsReportForm(request.POST)

    if not form.is_valid():
        logger.warning("Form failed validation: {}".format(form.errors))
        return render(request, 'SymptomsReport/report.html', {'form': form})

    response = HttpResponseRedirect(reverse('SymptomsReports:index'))

    try:
        # If the user cookie was set this is an existing user
        user_cookie = request.get_signed_cookie(constants.USER_COOKIE_NAME)
        user_cookie_hash = None

        if user_cookie:
            # Check if the user has already reported - if they have check that the cooldown period has passed
            logger.info("Found existing signed user cookie: {}".format(user_cookie))
            previous_symptoms_report = _get_previous_user_symptoms_report(user_cookie)

            if previous_symptoms_report:
                user_cookie_hash = previous_symptoms_report.user_cookie_hash

                if not _reporting_cooldown_passed(request, previous_symptoms_report):
                    return response
            else:
                user_cookie_hash = None
    except BadSignature as e:
        logger.error("Signed user cookie signature failed: {}".format(e))
        user_cookie = None
        user_cookie_hash = None
    except KeyError:
        logger.info("Could not find existing signed user cookie")
        user_cookie = None
        user_cookie_hash = None

    # If there was no existing user cookie or retrieving the cookie failed:
    # set a new user cookie so we know the user has answered already
    if not user_cookie:
        user_cookie = uuid.uuid4()
        logger.info("Created new signed user cookie: {}".format(user_cookie))
        response.set_signed_cookie(
            constants.USER_COOKIE_NAME, str(user_cookie))

    # If the user cookie hash doesn't exist create it
    if not user_cookie_hash:
        user_cookie_hash = _get_hashed_value(str(user_cookie), settings.HASHING_SALT)

    symptoms = form.cleaned_data.get('symptoms')
    postal_code = form.cleaned_data.get('postal_code')
    tested_for_covid19 = form.cleaned_data.get('tested_for_covid19')
    size_of_household = form.cleaned_data.get('size_of_household')
    sex = form.cleaned_data.get('sex')
    duration = form.cleaned_data.get('duration')
    age = form.cleaned_data.get('age')
    post_code_area = _get_safe_post_code_area(postal_code)
    created_at = timezone.now()

    # Get client User-Agent, IP and country according to GeoIP
    # Hash the User-Agent and IP for privacy, - save the hashes and the country name with the data.
    # This way we can attempt to sanitize trolls easier from the data.
    client_user_agent_hash = _get_hashed_value(_get_client_user_agent(request), settings.HASHING_SALT)
    client_ip_hash = _get_hashed_value(_get_client_ip(request), settings.HASHING_SALT)
    client_geoip_country_code = _get_client_country_code(request)
    fill_start_time = form.cleaned_data.get('fill_start_time')

    if fill_start_time:
        fill_time_sec = int((created_at - fill_start_time).total_seconds())
    else:
        fill_time_sec = None

    symptoms_report = SymptomsReport(
        post_code_area=post_code_area,
        tested_for_covid19=tested_for_covid19,
        size_of_household=size_of_household,
        age=age,
        sex=sex,
        duration=duration,
        created_at=created_at,
        user_cookie=user_cookie,
        fill_time_sec=fill_time_sec,
        user_cookie_hash=user_cookie_hash,
        client_user_agent_hash=client_user_agent_hash,
        client_ip_hash=client_ip_hash,
        client_geoip_country_code=client_geoip_country_code)
    symptoms_report.save()

    for symptom_name in symptoms:
        symptom = Symptom(
            name=symptom_name,
            symptoms_report=symptoms_report
        )
        symptom.save()

    return response


def list_municipality_data(request, municipality_code=None):
    report = _municipality_symptom_report(municipality_code=municipality_code)

    return JsonResponse({
        "report": report,
    })


def _municipality_symptom_report(municipality_code=None):
    symptom_counts_by_municipality = Symptom.objects\
        .values('symptoms_report__post_code_area__municipality__code', 'name')\
        .annotate(count=Count('name'))
    symptom_reports_counts_by_municipality = SymptomsReport.objects\
        .values('post_code_area__municipality__code')\
        .annotate(count=Count('post_code_area__municipality__code'))

    if municipality_code is not None:
        symptom_counts_by_municipality = symptom_counts_by_municipality.filter(
            symptoms_report__post_code_area__municipality__code=municipality_code)
        symptom_reports_counts_by_municipality = symptom_counts_by_municipality.filter(
            post_code_area__municipality__code=municipality_code)

    report = {}
    for record in symptom_counts_by_municipality:
        key = record["symptoms_report__post_code_area__municipality__code"]
        val = report.get(key, {"reports": {}, "symptoms": {}})
        val["symptoms"][record["name"]] = record["count"]
        report[key] = val
    for record in symptom_reports_counts_by_municipality:
        key = record["post_code_area__municipality__code"]
        val = report.get(key, {"reports": {}, "symptoms": {}})
        val["reports"]["count"] = record["count"]
        report[key] = val

    logger.debug(symptom_counts_by_municipality.query)
    logger.debug(symptom_reports_counts_by_municipality.query)

    return report


def _municipality_symptom_report_translations():
    return dict(Symptom.SymptomName.choices)


def _get_previous_user_symptoms_report(user_cookie: str) -> Union[SymptomsReport, None]:
    if user_cookie:
        try:
            previous_symptoms_report = SymptomsReport.objects.filter(user_cookie=user_cookie).order_by('-created_at')[0]
            logger.info("Found previous symptoms report for user: {}. Report: {}"
                        .format(user_cookie, previous_symptoms_report))
            return previous_symptoms_report
        except Exception as e:
            logger.error("Failed to get previous symptoms report for user: {}. Error: {}"
                         .format(user_cookie, e))
    return None


def _reporting_cooldown_passed(request, report: SymptomsReport) -> bool:
    if not flag_is_active(request, 'USE_SYMPTOM_REPORT_COOLDOWN'):
        return True

    delta = timezone.now() - report.created_at
    logger.info("Time since previous report: {} sec".format(delta.total_seconds()))

    if delta.total_seconds() < constants.SYMPTOM_REPORT_COOLDOWN_SEC:
        logger.warning("Reporting cooldown period of {} sec has not passed"
                       .format(constants.SYMPTOM_REPORT_COOLDOWN_SEC))
        return False

    return True


def _get_client_user_agent(request):
    return request.META.get('HTTP_USER_AGENT')


def _get_client_country_code(request):
    ip = _get_client_ip(request)
    if ip:
        return _GEO_IP2.country_code(ip)
    return None


def _get_client_ip(request):
    client_ip, is_routable = get_client_ip(request)

    # Unable to get the client's IP address
    if client_ip is None:
        return None
    else:
        # We got the client's IP address
        # The client's IP address is publicly routable on the Internet
        if is_routable:
            return client_ip
            # The client's IP address is private
        else:
            return None


def _get_safe_post_code_area(postal_code: str):
    global _POSTAL_CODE_TO_SAFE_POSTAL_CODE

    # Generate the mapping if it doesn't exist
    if not _POSTAL_CODE_TO_SAFE_POSTAL_CODE or len(_POSTAL_CODE_TO_SAFE_POSTAL_CODE) == 0:
        _generate_safe_postal_code_map()

    # If the value has a mapping it's not safe to use so replace it
    if postal_code in _POSTAL_CODE_TO_SAFE_POSTAL_CODE:
        logger.info("Mapping found: {} -> {}".format(postal_code, _POSTAL_CODE_TO_SAFE_POSTAL_CODE.get(postal_code)))
        safe_postal_code = _POSTAL_CODE_TO_SAFE_POSTAL_CODE.get(postal_code)
    else:
        safe_postal_code = postal_code

    return PostCodeArea.objects.get(postal_code=safe_postal_code)


def _generate_safe_postal_code_map():
    global _POSTAL_CODE_TO_SAFE_POSTAL_CODE

    logger.info("Creating safe postal code mapping with population limit: {}".format(
        constants.SAFE_POSTAL_CODE_POPULATION_LIMIT))

    for pca in PostCodeArea.objects.all():
        # If the population is less than the safety limit map to the most populous postal code within the
        # same municipality
        if pca.population < constants.SAFE_POSTAL_CODE_POPULATION_LIMIT:
            municipality_larget_pca = PostCodeArea.objects \
                .filter(municipality__code=pca.municipality.code).order_by("-population")[0]
            logger.debug("Postal code area: {} has population < {}. Mapped to: {} with popultion: {}".format(
                pca.postal_code, constants.SAFE_POSTAL_CODE_POPULATION_LIMIT, municipality_larget_pca.postal_code,
                municipality_larget_pca.population))
            _POSTAL_CODE_TO_SAFE_POSTAL_CODE[pca.postal_code] = municipality_larget_pca.postal_code
    logger.info("Safe postal code mapping created successfully with: {} entries".format(
        len(_POSTAL_CODE_TO_SAFE_POSTAL_CODE)))

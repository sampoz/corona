from django.urls import path

from . import views

app_name = 'SymptomsReports'
urlpatterns = [
    path('', views.index, name='index'),
    # ex: /SymptomsReports/5/
    path('SymptomsReports/<int:symptomreport_id>/', views.detail, name='detail'),
    # ex: SymptomsReports/report/
    path('report/', views.report, name='report'),
    # for post
    path('SymptomsReports/submitreport/',
         views.reportsubmit, name='reportsubmit'),
    path('api/reports/municipalities/<int:municipality_code>/', views.list_municipality_data,
         name='list_municipality_data'),
    path('api/reports/municipalities', views.list_municipality_data,
         name='list_municipality_data')
]

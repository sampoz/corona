import hashlib
from typing import Union


def _get_hashed_value(value: str, salt: str) -> Union[None, str]:
    if value is None:
        return None

    sha_512_hashed_value = hashlib.sha512((value + salt).encode('utf-8')).hexdigest()
    return sha_512_hashed_value

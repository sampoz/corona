from django.core.exceptions import ValidationError


def postal_code_validator(value: str):
    if len(value) == 5 and value.isdigit():
        return True
    else:
        raise ValidationError("Postal codes should be exactly 5 digits long".format(value))
��    K      t  e   �      `  �   a          %     .     7     @     I     R     [  
   h     s     �     �     �     �     �     �          %  
   2     =     I     P     W     r     �  "   �     �      �      	     (	     >	     ^	  .   z	  $   �	  +   �	  !   �	      
  #   =
  $   a
     �
  '   �
  (   �
  "   �
  &        =     M     [     h     x     �     �     �  	   �     �     �     �     �  
   �     	  	        !     4     P     l     ~     �     �     �     �     �     
          ,  k  C  �   �     v     �     �     �     �     �     �     �     �     �       "   "  5   E     {     �     �     �     �     �     �     �     �     �     �  	   �     �     �  	             $     0     =     B      Q  &   r  
   �     �     �     �  	   �     �  "        +     ?     ^     o     �     �     �     �     �     �     �     �     �  
             !  	   0  
   :     E     H     ]     r     �     �     �     �     �     �     �     �     
        3   *   $       %   #      1                     >   5          &      4   D   "   
   !   @   0      '          )          	      C   B   9      <          +      =   6   -              J              ?       A       8   F       H      .             /   ;               :   K                 (           E                        I                7   G         ,   2    
        You should only report your symptoms once in %(cooldown_period_hours)s hours.
        You still have %(cooldown_remaining_hours)s hours until the next reporting period.
         age12_17 age18_24 age25_34 age35_44 age45_54 age55_64 age65_74 age75OrOlder ageUnder12 formErrorInvalidPostalCode formErrorNoPostalCode formErrorNoSymptoms formErrorNoSymptomsCombined formFieldDuration formFieldPostalCode formFieldSizeOfHousehold formFieldSymptoms genderFemale genderMale genderOther langEN langFI modelFieldMunicipalityCode modelFieldMunicipalityName modelFieldPostCodeAreaArea modelFieldPostCodeAreaMunicipality modelFieldPostCodeAreaName modelFieldPostCodeAreaPopulation modelFieldPostCodeAreaPostalCode modelFieldSymptomName modelFieldSymptomSymptomsReport modelFieldSymptomsReportAge modelFieldSymptomsReportClientGeoIpCountryCode modelFieldSymptomsReportClientIpHash modelFieldSymptomsReportClientUserAgentHash modelFieldSymptomsReportCreatedAt modelFieldSymptomsReportDuration modelFieldSymptomsReportFillTimeSec modelFieldSymptomsReportPostCodeArea modelFieldSymptomsReportSex modelFieldSymptomsReportSizeOfHousehold modelFieldSymptomsReportTestedForCovid19 modelFieldSymptomsReportUserCookie modelFieldSymptomsReportUserCookieHash symptomANOREXIA symptomCHILLS symptomCOUGH symptomDIARRHEA symptomDIZZINESS symptomFATIGUE symptomFEVER symptomHEADACHE symptomMP symptomNASAL_CONGESTION symptomNAUSEA symptomNONE symptomOTHER symptomSOB symptomSPUTUM symptomST testedForCovid19No testedForCovid19YesNegative testedForCovid19YesPositive uiBackToFrontpage uiBackToFrontpageDescription uiReportCountry uiReportSymptoms uiReportSymptomsDescription uiReportSymptomsFormCTA uiReportSyptomsFormHeader uiReportsTotal uiReportsTotalZero uiThankYouForReporting Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-03-24 19:19+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 
        Voit raportoida oireesi vain kerran %(cooldown_period_hours)s tunnissa.
        Sinulla on vielä %(cooldown_remaining_hours)s tuntia jäljellä seuraavan raportointijakson alkuun.
         12-17 vuotta 18-24 vuotta 25-35 vuotta 35-44 vuotta 45-54 vuotta 55-64 vuotta 65-74 vuotta 75 vuotta tai vanhempi Alle 12 vuotta Postinumeroa ei löydy Täytä postinumero Valitse ainakin yksi oire listasta Et voi yhdistää 'ei oireita' muiden oireiden kanssa Päiviä oireiden alkamisesta Postinumero Ruokakunnan koko Oireet Nainen Mies Muu Englanti Suomi Kunnan koodi Kunta Pinta-ala Kunta Postikoodialue Väkiluku Postinumero Oireen nimi Oireraportti Ikä GeoIP maakoodi Päätteen IP-osoitteen tiiviste Päätteen käyttäjäagentin tiiviste Luomisaika Päiviä oireiden alkamisesta Raportin täyttöaika (sek) Postikoodialue Sukupuoli Ruokakunnan koko Onko sinulle tehty COVID-19 testi? Käyttäjätunniste Käyttäjätunnisteen tiiviste Ruokahaluttomuus Vilunväristykset Yskä Ripuli Huimaus/sekavuus Väsymys Kuume (yli 37.5C) Päänsärky Lihassärky/nivelsärky Nuha/tukkoinen nenä Pahoinvointi Ei oireita Muu listaamaton oire Hengenahdistus Limaisuus Kurkkukipu Ei Kyllä, negatiivinen Kyllä, positiivinen Palaa etusivulle Tarkastele raportointituloksia Suomi Raportoi oireesi Auta kartoituksessa Raportoi Raportoi oireesi Raportteja yhteensä Ei raportteja Kiitos raportoinnista 
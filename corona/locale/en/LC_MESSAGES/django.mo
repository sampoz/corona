��    K      t  e   �      `  �   a          %     .     7     @     I     R     [  
   h     s     �     �     �     �     �     �          %  
   2     =     I     P     W     r     �  "   �     �      �      	     (	     >	     ^	  .   z	  $   �	  +   �	  !   �	      
  #   =
  $   a
     �
  '   �
  (   �
  "   �
  &        =     M     [     h     x     �     �     �  	   �     �     �     �     �  
   �     	  	        !     4     P     l     ~     �     �     �     �     �     
          ,  k  C  �   �     j     z     �     �     �     �     �     �     �     �       )   +  4   U     �     �     �     �     �     �     �     �     �     �                      
        )     5     E     U     Y     s     �  
   �     �     �     �     �     �  "   �           ,     =     N     U     [     d     x     �     �     �     �     �     �     �     �               *     -     ;     I     [     y     �  *   �     �     �     �     �     �        3   *   $       %   #      1                     >   5          &      4   D   "   
   !   @   0      '          )          	      C   B   9      <          +      =   6   -              J              ?       A       8   F       H      .             /   ;               :   K                 (           E                        I                7   G         ,   2    
        You should only report your symptoms once in %(cooldown_period_hours)s hours.
        You still have %(cooldown_remaining_hours)s hours until the next reporting period.
         age12_17 age18_24 age25_34 age35_44 age45_54 age55_64 age65_74 age75OrOlder ageUnder12 formErrorInvalidPostalCode formErrorNoPostalCode formErrorNoSymptoms formErrorNoSymptomsCombined formFieldDuration formFieldPostalCode formFieldSizeOfHousehold formFieldSymptoms genderFemale genderMale genderOther langEN langFI modelFieldMunicipalityCode modelFieldMunicipalityName modelFieldPostCodeAreaArea modelFieldPostCodeAreaMunicipality modelFieldPostCodeAreaName modelFieldPostCodeAreaPopulation modelFieldPostCodeAreaPostalCode modelFieldSymptomName modelFieldSymptomSymptomsReport modelFieldSymptomsReportAge modelFieldSymptomsReportClientGeoIpCountryCode modelFieldSymptomsReportClientIpHash modelFieldSymptomsReportClientUserAgentHash modelFieldSymptomsReportCreatedAt modelFieldSymptomsReportDuration modelFieldSymptomsReportFillTimeSec modelFieldSymptomsReportPostCodeArea modelFieldSymptomsReportSex modelFieldSymptomsReportSizeOfHousehold modelFieldSymptomsReportTestedForCovid19 modelFieldSymptomsReportUserCookie modelFieldSymptomsReportUserCookieHash symptomANOREXIA symptomCHILLS symptomCOUGH symptomDIARRHEA symptomDIZZINESS symptomFATIGUE symptomFEVER symptomHEADACHE symptomMP symptomNASAL_CONGESTION symptomNAUSEA symptomNONE symptomOTHER symptomSOB symptomSPUTUM symptomST testedForCovid19No testedForCovid19YesNegative testedForCovid19YesPositive uiBackToFrontpage uiBackToFrontpageDescription uiReportCountry uiReportSymptoms uiReportSymptomsDescription uiReportSymptomsFormCTA uiReportSyptomsFormHeader uiReportsTotal uiReportsTotalZero uiThankYouForReporting Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-03-24 19:19+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 
        You should only report your symptoms once in %(cooldown_period_hours)s hours.
        You still have %(cooldown_remaining_hours)s hours until the next reporting period.
         12-17 years old 18-24 years old 25-34 years old 35-44 years old 45-54 years old 55-64 years old 65-74 years old 75 years or older Under 12 years old Invalid postal code Postal code is required Select at least one symptom from the list You cannot combine 'no symptoms' with other symptoms Days since start of symptoms Postal code Size of household Symptoms Female Male Other English Finnish Municipality code Name Area Municipality Name Population Postal code Name of symptom Symptoms report Age Client GeoIP country code Client IP hash Client user agent hash Created at Days since start of symptoms Report fill time (sec) Post code area Sex Size of household Have you been tested for COVID-19? User cookie User cookie hash Loss of appetite Chills Cough Diarrhea Dizziness/confusion Fatigue Fever (over 37.5C) Headache Muscle ache Nasal congestion/runny nose Nausea No symptoms Other unlisted symptoms Shortness of breath Increased mucus production Sore throat No Yes, negative Yes, positive Back to frontpage View the reporting statistics Finland Report symptoms Help to map symptoms across the population Report Report symptoms Total reports None Thank you for reporting 